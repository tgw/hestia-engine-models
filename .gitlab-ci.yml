default:
  image: python:3.9
  before_script:
    - pip install -r requirements.txt
    - pip install -r requirements-ci.txt

stages:
  - test-build
  - deploy
  - post-deploy

lint-commit:
  image: node:14
  stage: test-build
  before_script:
    - npm install
  script:
    - git fetch origin develop
    - npx commitlint --from=origin/develop
  except:
    - tags
    - develop
    - master

lint:
  stage: test-build
  before_script:
    - pip install flake8==4 flake8-pytest==1.3
  script:
    - flake8
  except:
    - tags
    - master

validate-jsonld:
  image: node:14
  stage: test-build
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID_RO
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY_RO
  before_script:
    - source envs/.dev.env
    - npm install
  script:
    - npm test
    # make sure terms exist
    - npm run build:models
    - npm run test:terms
  except:
    - tags
    - master

test:
  stage: test-build
  script:
    - source envs/.dev.env
    - pip install pytest-cov pytest-xdist coverage
    - pytest -n 5 --cov-config=.coveragerc --cov-report term --cov-report html --cov-report xml --cov=./ --cov-fail-under=95
  coverage: /Total coverage\s*:\s*([^%]+)/
  artifacts:
    paths:
      - coverage/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
  except:
    - tags
    - master

build-mocking:
  stage: test-build
  script:
    - source envs/.${CI_COMMIT_REF_NAME}.env || source envs/.master.env
    - python build_mocking.py search-results.json
  artifacts:
    paths:
      - search-results.json
  only:
    - develop@hestia-earth/hestia-engine-models
    - master@hestia-earth/hestia-engine-models
    - tags@hestia-earth/hestia-engine-models

deploy-dev:
  stage: deploy
  script:
    - cp search-results.json hestia_earth/models/mocking/.
    - python setup.py sdist bdist_wheel
    - pip install twine
    - twine upload -u $PYPI_USERNAME -p $PYPI_TOKEN_TEST --repository-url https://test.pypi.org/legacy/ dist/* || true
    - twine upload -u npm -p $GITLAB_NPM_TOKEN --repository-url https://$GITLAB_PYPI_URL dist/* || true
  only:
    - develop@hestia-earth/hestia-engine-models

deploy:
  stage: deploy
  script:
    - cp search-results.json hestia_earth/models/mocking/.
    - python setup.py sdist bdist_wheel
    - pip install twine
    - twine upload -u $PYPI_USERNAME -p $PYPI_TOKEN --skip-existing dist/*
  only:
    - tags@hestia-earth/hestia-engine-models

deploy-docs:
  stage: deploy
  trigger:
    project: hestia-earth/hestia-api-documentation
    branch: ${CI_COMMIT_REF_NAME}
  only:
    - develop@hestia-earth/hestia-engine-models
    - master@hestia-earth/hestia-engine-models

build-layer:
  image: docker:stable
  stage: deploy
  services:
    - docker:dind
  artifacts:
    paths:
      - layer/python
  before_script:
    - chmod +x layer/build.sh
  script:
    - ./layer/build.sh
  only:
    - develop@hestia-earth/hestia-engine-models
    - master@hestia-earth/hestia-engine-models

deploy-layer:
  stage: post-deploy
  before_script:
    - source envs/.${CI_COMMIT_REF_NAME}.env
    - pip install awscli
    - aws s3 cp ${ECOINVENT_FILE_LOCATION} ./layer/python/lib/python3.9/site-packages/hestia_earth/models/data/ecoinventV3/ecoinventV3_excerpt.csv
    - apt-get update && apt-get install -y zip
    - chmod +x layer/deploy.sh
  script:
    - ./layer/deploy.sh $STAGE
  only:
    - develop@hestia-earth/hestia-engine-models
    - master@hestia-earth/hestia-engine-models

trigger-engine-reconciliation:
  stage: post-deploy
  trigger:
    project: hestia-earth/hestia-engine-reconciliation
    branch: master
  only:
    - tags@hestia-earth/hestia-engine-models
