## Transformations

Returns the `Emission` from every `Transformation` to be added in the `Cycle`.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [value](https://hestia.earth/schema/Emission#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - optional:
    - a list of [transformations](https://hestia.earth/schema/Cycle#transformations) with:
      - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
        - [value](https://hestia.earth/schema/Input#value)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('transformation', Cycle))
```
