## Completeness Animal feed

This model checks if we have the requirements below and updates the
[Data Completeness](https://hestia.earth/schema/Completeness#animalFeed) value.

### Returns

* A [Completeness](https://hestia.earth/schema/Completeness) with:
  - [animalFeed](https://hestia.earth/schema/Completeness#animalFeed)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for animalFeed: [completeness.animalFeed](https://hestia.earth/schema/Completeness#animalFeed) must be `False`
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('completeness.animalFeed', Cycle))
```
