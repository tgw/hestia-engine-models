## Product Value

This model calculates the `value` of the [Product](https://hestia.earth/schema/Product)
by taking an average from the `min` and `max` values.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [min](https://hestia.earth/schema/Product#min) and [max](https://hestia.earth/schema/Product#max)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.value', Cycle))
```
