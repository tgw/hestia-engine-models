# Schmidt (2007)

These models calculate various emissions, primarily related to palm oil production and processing, according to the models described in [Schmidt (2007)](https://lca-net.com/publications/show/life-cycle-assessment-rapeseed-oil-palm-oil-ph-d-thesis-part-3-life-cycle-inventory-rapeseed-oil-palm-oil/).
