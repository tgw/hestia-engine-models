# ReCiPe 2016 Hierarchist

These models characterise emissions and resource uses according to the ReCiPe 2016 method, using a hierarchist perspective (see [Huijbregts et al (2016)](https://www.rivm.nl/bibliotheek/rapporten/2016-0104.pdf)).
