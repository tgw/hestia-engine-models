## Human damage ozone formation

The potential of emissions to contribute to the increase in tropospheric ozone population intake.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [humanDamageOzoneFormation](https://hestia.earth/term/humanDamageOzoneFormation)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Individualist](https://hestia.earth/term/recipe2016Individualist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for pesticidesAntibiotics: [completeness.pesticidesAntibiotics](https://hestia.earth/schema/Completeness#pesticidesAntibiotics) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [pesticideAI](https://hestia.earth/glossary?termType=pesticideAI)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `noxEqIndividualistHumanDamageOzoneFormationReCiPe2016`
- [pesticideAI.csv](https://hestia.earth/glossary/lookups/pesticideAI.csv) -> `noxEqIndividualistHumanDamageOzoneFormationReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Individualist import run

print(run('humanDamageOzoneFormation', ImpactAssessment))
```
