## CH4, to air, flooded rice

Methane emissions to air, from flooded paddy rice fields.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [ch4ToAirFloodedRice](https://hestia.earth/term/ch4ToAirFloodedRice)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [min](https://hestia.earth/schema/Emission#min)
  - [max](https://hestia.earth/schema/Emission#max)
  - [sd](https://hestia.earth/schema/Emission#sd)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) with [ricePlantFlooded](https://hestia.earth/term/ricePlantFlooded) **or** [riceGrainInHuskFlooded](https://hestia.earth/term/riceGrainInHuskFlooded)
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [croppingDuration](https://hestia.earth/term/croppingDuration)
  - optional:
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser)
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated)
    - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
      - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [cropResidueManagement](https://hestia.earth/glossary?termType=cropResidueManagement)
      - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [waterRegime](https://hestia.earth/glossary?termType=waterRegime)

### Lookup used

- [waterRegime.csv](https://hestia.earth/glossary/lookups/waterRegime.csv) -> `IPCC_2019_CH4_rice_SFw`; `IPCC_2019_CH4_rice_SFw_min`; `IPCC_2019_CH4_rice_SFw_max`; `IPCC_2019_CH4_rice_SFw_sd`; `IPCC_2019_CH4_rice_SFp`; `IPCC_2019_CH4_rice_SFp_min`; `IPCC_2019_CH4_rice_SFp_max`; `IPCC_2019_CH4_rice_SFp_sd`
- [organicFertiliser.csv](https://hestia.earth/glossary/lookups/organicFertiliser.csv) -> `IPCC_2019_CH4_rice_CFOA_kg_fresh_weight`; `IPCC_2019_CH4_rice_CFOA_kg_dry_weight`
- [region-ch4ef-IPCC2019.csv](https://hestia.earth/glossary/lookups/region-ch4ef-IPCC2019.csv) -> `CH4_ef`; `CH4_ef_min`; `CH4_ef_max`; `CH4_ef_sd`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `productTermIdsAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('ch4ToAirFloodedRice', Cycle))
```
