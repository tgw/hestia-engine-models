# Akagi et al (2011) and IPCC (2006)

This model calculates the emissions from crop residue burning using the methodology detailed in the [IPCC 2006 guidelines](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_02_Ch2_Generic.pdf) (Volume 4, Chapter 2, Section 2.4) and the emissions factors detailed in [Akagi et al (2011)](https://doi.org/10.5194/acp-11-4039-2011).
