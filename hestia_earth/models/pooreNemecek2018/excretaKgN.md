## Excreta (kg N)

This model uses a mass balance to calculate the total amount of excreta (as N) created by animals.
The inputs into the mass balance are the total amount of feed and the total amount of net primary production
in the water body.
The outputs of the mass balance are the weight of the animal and the excreta.
The formula is excreta = feed + NPP - animal.

For [live aquatic species](https://hestia.earth/glossary?termType=liveAquaticSpecies), if the mass balance fails
(i.e. [animal feed](https://hestia.earth/schema/Completeness#animalFeed) is not complete, see requirements below),
a simplified formula is used: total nitrogen content of the fish * 3.31.
See [Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987) for further details.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for animalFeed: [completeness.animalFeed](https://hestia.earth/schema/Completeness#animalFeed)
  - Data completeness assessment for products: [completeness.products](https://hestia.earth/schema/Completeness#products)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [feedFoodAdditive](https://hestia.earth/glossary?termType=feedFoodAdditive) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` and [value](https://hestia.earth/schema/Input#value) `> 0` and [isAnimalFeed](https://hestia.earth/schema/Input#isAnimalFeed) with `True` and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [crudeProteinContent](https://hestia.earth/term/crudeProteinContent)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [liveAquaticSpecies](https://hestia.earth/glossary?termType=liveAquaticSpecies) and optional:
      - a list of [properties](https://hestia.earth/schema/Product#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)

### Lookup used

- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `nitrogenContent`; `crudeProteinContent`
- [animalProduct.csv](https://hestia.earth/glossary/lookups/animalProduct.csv) -> `excretaKgNTermId`
- [liveAnimal.csv](https://hestia.earth/glossary/lookups/liveAnimal.csv) -> `excretaKgNTermId`
- [liveAquaticSpecies.csv](https://hestia.earth/glossary/lookups/liveAquaticSpecies.csv) -> `excretaKgNTermId`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('excretaKgN', Cycle))
```
