## Flowing Water

This model returns a measurement of fast flowing water or slow flowing water depending on the type of the site.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [slowFlowingWater](https://hestia.earth/term/slowFlowingWater) **or** [fastFlowingWater](https://hestia.earth/term/fastFlowingWater)
  - [value](https://hestia.earth/schema/Measurement#value) with `1`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('flowingWater', Site))
```
