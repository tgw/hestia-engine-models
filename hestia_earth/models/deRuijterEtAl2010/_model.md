# de Ruijter et al (2010)

This model calculates the NH3 emissions due to crop residue decomposition using the regression model in [de Ruijter et al (2010)](https://doi.org/10.1016/j.atmosenv.2010.06.019).
