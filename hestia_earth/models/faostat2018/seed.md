## Seed

The seed of a crop.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [term](https://hestia.earth/schema/Input#term) with [seed](https://hestia.earth/term/seed)
  - [methodModel](https://hestia.earth/schema/Input#methodModel) with [faostat2018](https://hestia.earth/term/faostat2018)
  - [value](https://hestia.earth/schema/Input#value)
  - [sd](https://hestia.earth/schema/Input#sd)
  - [statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `regions`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) and [value](https://hestia.earth/schema/Product#value) `> 0`
  - Data completeness assessment for other: [completeness.other](https://hestia.earth/schema/Completeness#other) must be `False`

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `seedPerKgYield`; `seedPerKgYield-sd`
- [other.csv](https://hestia.earth/glossary/lookups/other.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.faostat2018 import run

print(run('seed', Cycle))
```
