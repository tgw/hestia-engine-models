# Scherer Pfister (2015)

This model implements the phosphorus emissions models detailed in [Scherer & Pfister (2015)](https://doi.org/10.1007/s11367-015-0880-0), many of which were originally developed in the [SALCA guidelines](https://www.agroscope.admin.ch/agroscope/en/home/topics/environment-resources/life-cycle-assessment/salca-method.html).
